# -*- coding: utf-8 -*-
"""
Created on Sat May 25 12:15:17 2024

@author: benjamin_arrondeau (@arrondeb)
"""

import requests
import sys
import json
import numpy as np


# functions
def message_to_search(mode, build_message, pack_message):
    if mode == "build":
        return build_message
    if mode == "pack":
        return pack_message

def find_last_pipeline_id(url, message):
    # In reality, we should search over the last pipelines

    # Get the lasts 100 pipelines
    params = {'per_page': '100'}
    r = requests.get(url, params)
    r = r.json()

    # Iterate through the pipelines and get their last job
    # Then, we look for a job whose commit title is the message
    params = {'per_page': '1',
              'scope[]': ['success', 'failed']}
    for pipeline in r:
        url_jobs = url + "/" + str(pipeline['id']) + "/jobs"
        new_r = requests.get(url_jobs, params)
        job = new_r.json()
        if len(job)>0:
            commit_title = job[0]['commit']['title']
            if commit_title == message:
                return pipeline['id']
            
    # If nothing is found, we throw an error
    raise Exception(f"No pipeline found with commit message: {message}")

def parse_inputs():
    project_id = sys.argv[1]
    mode = sys.argv[2]
    first_param = sys.argv[3]
    second_param = sys.argv[4]

    return project_id, mode, first_param, second_param


# Core code
# These following lines are setting the strings we are looking for in commit names
build_message = "[build] Commit from gitlab-ci"
pack_message = "[pack] Commit from gitlab-ci"

# Get the first and second badge name by parsing input params
project_id, mode, first_param, second_param = parse_inputs()

# Get the first and second badge name by parsing input params
message = message_to_search(mode, build_message, pack_message)

# This URL is pointing to the repo
# https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix-packages/
# The ID (26601) is referring to this repo (got it through prior API requests)
url = "https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/" + project_id + "/pipelines"

# Get the ID of the last ran pipeline
# It is required to then get the jobs list
id_pipeline = find_last_pipeline_id(url, message)

# Build the URL and params for the request
url_jobs = url + "/" + str(id_pipeline) + "/jobs"
params = {'per_page': '100',
          'scope[]': ['success', 'failed']}

# Get all jobs from the pipeline and format the result to a JSON array
r = requests.get(url_jobs, params)
r = r.json()

# Compute the percentage of success of the pipeline
# Roughly (number of success) / (total number of jobs)
number_jobs = len(r)
number_success = 0
number_tasks = np.count_nonzero([mode in job["name"] for job in r])
for i in range(number_jobs):
    job = r[i]
    if (mode in job["name"]) and (job["status"] == "success"):
        number_success+=1

# This line enables to get the result as 97% instead of 97.0%
ratio = "{:.0f}".format(100 * number_success/number_tasks)

with open("status.json", "r") as jsonFile:
    data = json.load(jsonFile)

data[first_param] = str(ratio) + "%"
data[second_param] = number_tasks

with open("status.json", "w") as jsonFile:
    json.dump(data, jsonFile)
