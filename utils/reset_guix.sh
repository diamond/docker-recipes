# First, we reset the folders
if [ -d gnu_store/ ]; then
   rm -rf gnu_store
fi

if [ -d var_guix/ ]; then
   rm -rf var_guix
fi

mkdir -p gnu_store/
mkdir -p var_guix/

# Then we initialize the folders (i.e. /gnu/store and /var/guix)
docker run --privileged --mount type=bind,source="$(pwd)"/gnu_store,target=/gnu/store --mount type=bind,source="$(pwd)"/var_guix,target=/var/guix benji12358/guix:with_non_guix tar -xf guix-binary-1.4.0.x86_64-linux.tar.xz

# Finally, we update guix (we just need to change the commit)
docker run --privileged --mount type=bind,source="$(pwd)"/gnu_store,target=/gnu/store --mount type=bind,source="$(pwd)"/var_guix,target=/var/guix benji12358/guix:with_non_guix /entry-point.sh /bin/bash -c "guix pull --commit=bef48dd553aef3c8fb3bc48143739debb629c825; guix gc; guix gc --optimize"
