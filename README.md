# Docker recipes used for the DIAMOND project of PEPR DIADEM

The goal of this repository is to provide the recipes of all the docker images used in the DIAMOND project.

## HowTo build an image

To build a specific docker image, just run the following command:

```
docker build -t image_name:tag -f Dockerfile_image_name .
```

## HowTo publish an image

Once you have built the docker image, you can publish it to the dockerhub (and thus, use it in the different CI/CD pipelines). This can done by running the following command:

```
docker tag image_name:tag docker_username/image_name:tag
docker push docker_username/image_name:tag
``` 
